<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;

class PostController extends Controller
{
    public function index()
    {
        return Post::all();
    }

    public function detail($id)
    {
        $post = Post::find($id);
        if (!$post) {
            return response()->json([
                'post' => 'Resource not found',
            ], Response::HTTP_NOT_FOUND);
        }
        $post->views++;
        $post->save();
        return $post;
    }

    public function create(Request $request)
    {
        $post = Post::create($request->all());
        $this->uploadImage($post, $request);
        return $post;
    }

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->all());
        $this->uploadImage($post, $request);
        return $post;
    }

    public function delete(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return 204;
    }

    private function uploadImage($post, Request $request) {
        $file = $request->file('image');
        if ($file) {
            $ext = $file->getClientOriginalExtension();
            $filename = uniqid() . '.' . $ext;
            $path = public_path('uploads/');
            $file->move($path, $filename);
            $post->image = 'uploads/' . $filename;
            $post->save();
        }
    }
}
