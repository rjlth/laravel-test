<?php

use Illuminate\Http\Request;
use App\Post;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('posts', 'PostController@index');
Route::get('posts/{id}', 'PostController@detail');
Route::post('posts', 'PostController@create');
Route::put('posts/{id}', 'PostController@update');
Route::delete('posts/{id}', 'PostController@delete');